CREATE TABLE product
(
    product_id  INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name        VARCHAR(255)   NOT NULL,
    description TEXT           NOT NULL,
    vendor_code VARCHAR(100)   NOT NULL UNIQUE,
    color       VARCHAR(50)    NOT NULL,
    size        VARCHAR(50)    NOT NULL,
    weight      DECIMAL(10, 2) NOT NULL
);

CREATE TABLE product_media
(
    media_id   INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    product_id INT          NOT NULL,
    media_type VARCHAR(50)  NOT NULL,
    media_url  VARCHAR(255) NOT NULL,
    FOREIGN KEY (product_id) REFERENCES product (product_id) ON DELETE CASCADE
);

CREATE TABLE question
(
    question_id        INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    product_id         INT  NOT NULL,
    name               VARCHAR(255),
    email              VARCHAR(255) CHECK (email ~ '^\w+(\.\w{2,})*@\w+(\.\w{2,})+$'),
    question_text      TEXT NOT NULL,
    parent_question_id INT,
    parent_answer_id   INT,
    FOREIGN KEY (product_id) REFERENCES product (product_id) ON DELETE CASCADE,
    FOREIGN KEY (parent_question_id) REFERENCES question (question_id) ON DELETE CASCADE
);

CREATE TABLE answer
(
    answer_id   INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    question_id INT  NOT NULL UNIQUE,
    answer_text TEXT NOT NULL,
    FOREIGN KEY (question_id) REFERENCES question (question_id) ON DELETE CASCADE
);
ALTER TABLE question
    ADD CONSTRAINT fk_question_parent_answer
        FOREIGN KEY (parent_answer_id) REFERENCES answer (answer_id) ON DELETE CASCADE;

CREATE TABLE review
(
    review_id         INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    product_id        INT         NOT NULL,
    phone_number      VARCHAR(20) NOT NULL CHECK (
        phone_number ~ '^(?:\+?7|8)?[-\s]?(\(?\d{3}\)?)[-\s]?\d{3}[-\s]?\d{2}[-\s]?\d{2}$'),
    confirmation_code VARCHAR(20) NOT NULL,
    rating            INT CHECK (rating >= 1 AND rating <= 5),
    comment           TEXT,
    FOREIGN KEY (product_id) REFERENCES product (product_id) ON DELETE CASCADE
);

CREATE TABLE review_feedback
(
    feedback_id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    review_id   INT     NOT NULL,
    is_helpful  BOOLEAN NOT NULL,
    FOREIGN KEY (review_id) REFERENCES review (review_id) ON DELETE CASCADE
);

CREATE TABLE category
(
    category_id        INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name_category      VARCHAR(255) NOT NULL,
    parent_category_id INT,
    FOREIGN KEY (parent_category_id) REFERENCES category (category_id) ON DELETE CASCADE
);

CREATE TABLE product_category
(
    product_id  INT NOT NULL,
    category_id INT NOT NULL,
    PRIMARY KEY (product_id, category_id),
    FOREIGN KEY (product_id) REFERENCES product (product_id) ON DELETE CASCADE,
    FOREIGN KEY (category_id) REFERENCES category (category_id) ON DELETE CASCADE
);