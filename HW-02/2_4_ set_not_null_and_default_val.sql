SELECT COUNT(*)
FROM book
WHERE author IS NULL;

ALTER TABLE book
    ALTER COLUMN author SET NOT NULL;

SELECT COUNT(*)
FROM customer
WHERE phone IS NULL;

UPDATE customer
SET phone = '00000000000'
WHERE phone IS NULL;

ALTER TABLE customer
    ALTER COLUMN phone SET NOT NULL;

SELECT COUNT(*)
FROM customer
WHERE email IS NULL;

UPDATE customer
SET email = 'empty@email.com'
WHERE email IS NULL;

ALTER TABLE customer
    ALTER COLUMN email SET NOT NULL;

