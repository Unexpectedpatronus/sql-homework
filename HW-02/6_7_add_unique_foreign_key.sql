ALTER TABLE book
    ADD COLUMN author_id INT,
    ADD COLUMN genre_id  INT;

UPDATE book b
SET author_id = a.author_id
FROM author a
WHERE b.author = a.name_author;

UPDATE book b
SET genre_id = g.genre_id
FROM genre g
WHERE b.genre = g.name_genre;

ALTER TABLE book
    ADD CONSTRAINT fk_author_name FOREIGN KEY (author_id) REFERENCES author (author_id) ON DELETE CASCADE,
    ALTER COLUMN author_id SET NOT NULL,
    DROP COLUMN author,
    ADD CONSTRAINT fk_genre FOREIGN KEY (genre_id) REFERENCES genre (genre_id) ON DELETE SET NULL,
    ALTER COLUMN genre_id SET NOT NULL,
    DROP COLUMN genre;
