SELECT *
FROM customer
WHERE NOT phone ~ '^(?:\+?7|8)?[-\s]?(\(?\d{3}\)?)[-\s]?\d{3}[-\s]?\d{2}[-\s]?\d{2}$'
   OR NOT email ~ '^\w+(\.\w{2,})*@\w+(\.\w{2,})+$';


ALTER TABLE customer
    ADD CONSTRAINT phone_number_format CHECK (
        phone ~ '^(?:\+?7|8)?[-\s]?(\(?\d{3}\)?)[-\s]?\d{3}[-\s]?\d{2}[-\s]?\d{2}$'),
    ADD CONSTRAINT email_format CHECK (email ~ '^\w+(\.\w{2,})*@\w+(\.\w{2,})+$');
