DROP TABLE IF EXISTS author CASCADE;
CREATE TABLE IF NOT EXISTS author
(
    name_author VARCHAR
);


DROP TABLE IF EXISTS genre CASCADE;
CREATE TABLE IF NOT EXISTS genre
(
    name_genre VARCHAR
);


DROP TABLE IF EXISTS book CASCADE;
CREATE TABLE IF NOT EXISTS book
(
    author VARCHAR,
    genre  VARCHAR,
    title  VARCHAR,
    price  DECIMAL,
    amount INT
);


DROP TABLE IF EXISTS supply;
CREATE TABLE IF NOT EXISTS supply
(
    author VARCHAR,
    title  VARCHAR,
    price  DECIMAL,
    amount INT
);


DROP TABLE IF EXISTS customer;
CREATE TABLE IF NOT EXISTS customer
(
    name VARCHAR,
    phone  VARCHAR,
    email  VARCHAR
);
